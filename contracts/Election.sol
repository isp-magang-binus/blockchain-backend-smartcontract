pragma solidity ^0.4.17;

contract Election {
    struct Candidate {
        string name;
        uint256 voteCount;
    }
    struct Voter {
        bool voted;
        uint256 voteIndex;
    }
    address[] public whitelist = [
        0xCA35b7d915458EF540aDe6068dFe2F44E8fa733c, //Maykel
        0x1708Be7e8aE42FbcF26D4DAC94d1561d099E6dFC, //Bob
        0xb0750D7C9C5E20586C7B88bb2eA79Cb49B030351, //Bagas
        0x0C7b6DF418876a31536De3cb2A3fE7A806B9617e //Maykel2
    ];
    address public owner;
    string public name;
    mapping(address => Voter) public voters;
    Candidate[] public candidates;
    uint256 public auctionEnd;
    uint256 public totalVotes;
    string valOwner = "You are not the Owner";

    event ElectionResult(string name, uint256 voteCount);

    function Election(
        string _name, //uint durationMinutes,
        string candidate1,
        string candidate2
    ) public {
        owner = msg.sender;
        name = _name;
        //auctionEnd = now +(durationMinutes * 1 minutes);

        candidates.push(Candidate(candidate1, 0));
        candidates.push(Candidate(candidate2, 0));
    }

    modifier restricted() {
        require(msg.sender == owner);
        _;
    }

    // function authorize(address voter) public restricted{
    //     require(!voters[voter].voted);

    //     voters[voter].weight = 1;
    // }
    function vote(uint256 voteIndex) {
        // require(now < auctionEnd);
        require(!voters[msg.sender].voted);
        for (uint256 i = 0; i < whitelist.length; i++) {
            if (msg.sender == whitelist[i]) {
                voters[msg.sender].voted = true;
                voters[msg.sender].voteIndex = voteIndex;

                candidates[voteIndex].voteCount += 1;
                totalVotes += 1;
            }
        }
    }

    function end() public {
        require(msg.sender == owner);
        //require(now >= auctionEnd);

        for (uint256 i = 0; i < candidates.length; i++) {
            ElectionResult(candidates[i].name, candidates[i].voteCount);
        }
    }
    function getArray() public view returns (address[] memory) {
        return whitelist;
    }
    function getAddress() public view returns (address) {
        return msg.sender;
    }

    function validateVoterOrOwner() public view returns (string) {
        if (msg.sender == owner) {
            return "Owner";
        } else
            for (uint256 i = 0; i < whitelist.length; i++) {
                if (msg.sender == whitelist[i]) {
                    return "Voter";
                }
            }
    }

    // function validateVoter()public view returns (string){
    //     for (uint i = 0 ; i<whitelist.length ; i++){
    //         if(msg.sender == whitelist[i]){
    //           return "Success";
    //         }
    //     }
    //     return "You are not a voter";
    // }

}
