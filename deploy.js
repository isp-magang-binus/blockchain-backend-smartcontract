const HDWalletProvider = require("truffle-hdwallet-provider");
const Web3 = require("web3");
const { interface, bytecode } = require("./compile");

const provider = new HDWalletProvider(
  "giant blur fancy talk junior remind door permit renew flag cinnamon photo",
  "https://rinkeby.infura.io/v3/077756f3b2364c178098f1732966ed76"
);

const web3 = new Web3(provider);

const deploy = async () => {
  const accounts = await web3.eth.getAccounts();
  console.log("Attempting to deploy from account", accounts[0]);
  const result = await new web3.eth.Contract(JSON.parse(interface))
    .deploy({ data: "0x" + bytecode, arguments: ["Nama", "Joko", "Bowo"] }) // add 0x bytecode
    .send({ from: accounts[0] }); // remove 'gas

  console.log(interface);
  console.log("Contract deployed to", result.options.address);
};

deploy();
