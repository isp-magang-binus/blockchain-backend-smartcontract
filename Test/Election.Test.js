const assert = require("assert");
const ganache = require("ganache-cli");
const Web3 = require("web3");
const provider = ganache.provider();
const web3 = new Web3(provider);
const { interface, bytecode } = require("../compile");

let accounts;
let election;
beforeEach(async () => {
  accounts = await web3.eth.getAccounts();

  election = await new web3.eth.Contract(JSON.parse(interface))
    .deploy({ data: bytecode, arguments: ["Nama", "5", "Joko", "Bowo"] })
    .send({ from: accounts[0], gas: "1000000" });
});

describe("Election", () => {
  it("Deployed", () => {
    //console.log(election);
    assert.ok(election.options.address);
  });
});
